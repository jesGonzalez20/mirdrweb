import Slider from './slider';

let sliderText = document.querySelector("#slider-text");
let sliderTitle = document.querySelector("#slider-title");
let sliderSubtitle = document.querySelector("#slider-subtitle");
let sliderImage = document.querySelector("#slider-image");


let slider = new Slider({
    elements: [
        {
            title: 'Lorem Ipsum',
            subtitle: 'Ipsum',
            image: 'https://images.pexels.com/photos/2263436/pexels-photo-2263436.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam corporis, itaque facere sequifugiat ducimus aperiam at non asperiores illum reiciendis a sunt, tempora molestias possimusmagnam beatae repellendus dolor?'
        },
        {
            title: 'Lorem Ipsum 2',
            subtitle: 'Ipsum 2',
            image: 'https://images.pexels.com/photos/1543767/pexels-photo-1543767.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
            text: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Veniam corporis, itaque facere sequifugiat ducimus aperiam at non '
        }
    ],
    animationFunction: function (element) {
        sliderTitle.innerHTML = element.title
        sliderSubtitle.innerHTML = element.subtitle;
        sliderText.innerHTML = element.text;
        sliderImage.src = element.image;

    },
    speed: 2000
});

slider.play();